# How to spawn a Wallabag server using a Traefik proxy and a self-hosted and not "containerized" database

## Requirements

* A functional [Docker engine](https://docs.docker.com/engine/install/)
* A functional `docker-compose` [command line](https://docs.docker.com/compose/install/)
* A functional [MySQL](https://www.mysql.com/) or [MariaDB](https://mariadb.org/) server reachabled from a running Docker container through a unix socket
* [optional] A functional [Redis](https://redis.io/) server reachabled from a running Docker container through a unix socket

## Information

This project relies on the usage of the following repositories:

* [wallabag/docker](https://github.com/wallabag/docker) to build a `customized` Wallabag Docker image
* [jlecorre/docker-traefik](https://gitlab.com/jlecorre/docker-traefik) to spawn and configure a Traefik proxy server

For more information related to the Wallabag project: [Wallabag on Github](https://github.com/wallabag/wallabag)

## Why don't used the official Docker image

It's pretty simple, I don't want to host a MariaDB server in a container because the server used to host this container already exposed one and I already had some useful scripts to backups databases, tables and data.  

To do this, I builded a new Docker image based on the official one with required instructions to allow the Wallabag server to use my own MariaDB database instead of waiting an hypothetic MariaDB/MySQL process running in a container started by the officiel `docker-compose.yml` file described [here](https://github.com/wallabag/docker#docker-compose).

### List of modifications that I made in the `Dockerfile` of the official project

* Edit of the `/etc/php7/php.ini` configuration file to prevent the following error:

```bash
(HY000/2002): No such file or directory in /var/www/html/core/db/mysqli.class.php on line 205
# as we used the mysql socket (already mounted in the container), we need to properly configure the php runtime to use it
```

* Edit of the `/etc/ansible/templates/parameters.yml.j2` file to "force" the usage of the MariaDB unix socket exposed by my server instead of the TCP protocole
* Edit of the `/etc/ansible/templates/parameters.yml.j2` file to "force" the usage of the `database_table_prefix` variable defined in the `wallabag.env` file because this configuration key is not interpreted during the service installation
* Edit of the `/etc/ansible/entrypoint.yml` file to deactivate the usage of a mysql server running in a Docker container in the Ansible tasks executed during the service bootstrapping steps

## How to start the Wallabag server

### Configure the `wallabag.env` file

/!\ This step is mandatory and critical. If your Wallabag doesn't worked, this is certainly due to a miss defined variable in this file /!\  

Edit and modify the `wallabag.env` file available in the repository to feat your environment.  
With this setup, the Wallabag component is configured to "automagically" connect to the local database exposed through a Unix socket by the server used to host the running Docker container.  
I also used a Redis server running on my host server and not in a container.

__Don't forget__ to check the [jlecorre/docker-traefik](https://gitlab.com/jlecorre/docker-traefik) to properly configured the required Traefik labels.

### Configure the `docker-compose` file

Unless some specific changes, the `docker-compose.yml` doesn't need to be modified.

### Configure your domain name

Depending on your DNS provider, it's up to you to setup the domain name used to publicly expose your Wallabag server.

### Build the required Docker image

```bash
docker-compose --env-file wallabag.env -f docker-compose.yml build
```

### Start the Docker container

```bash
docker-compose --env-file wallabag.env -f docker-compose up -d [--build] [--force-recreate]
```

### Get the Wallabag's container logs

```bash
docker-compose --env-file wallabag.env -f docker-compose logs -f
```

### Delete all resources related to this repository

```bash
docker-compose --env-file wallabag.env -f docker-compose.yml down --remove-orphans
# and don't forget to delete the Wallabag database previously created by your own means
```

## Useful information

- The Wallabag server can take several minutes to be started for the first boot due to the database population script.
- Don't forget to backup your database by your own means.
- A `Makefile` is available in this repository to easily operate the Docker Compose service.

