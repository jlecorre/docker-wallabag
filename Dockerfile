#
## @brief Customized Wallabag Docker image
## @author Joel LE CORRE <gitlab@sublimigeek.fr>
#

# Based on the official Wallabag image
# https://hub.docker.com/r/wallabag/wallabag
FROM wallabag/wallabag:2.5.4

# Environment variables
# https://github.com/wallabag/docker/releases
ENV WALLABAG_VERSION 2.5.4
ENV PHP_VERSION 8.1

# System update
RUN \
  apk update && \
  apk upgrade

# Edit manually the following files to:
# ** php.ini configuration file to prevent the following error
# (HY000/2002): No such file or directory in /var/www/html/core/db/mysqli.class.php on line 205
# as we used the mysql socket (already mounted in the container), we need to properly configure the php runtime to use it
# ** parameters.yml to "force" the usage of the MySQL unix socket instead of the TCP protocole and to change the table prefix
# because this configuration key is not interpreted during the service installation
# ** entrypoint.sh to deactivate the usage of waiting_database function in entrypoint
RUN \
  sed -i 's#mysqli.default_socket =#mysqli.default_socket = /run/mysqld/mysqld.sock#' /etc/php81/php.ini && \
  sed -i 's#pdo_mysql.default_socket=#pdo_mysql.default_socket = /run/mysqld/mysqld.sock#' /etc/php81/php.ini && \
  sed -i 's#database_socket: null#database_socket: /run/mysqld/mysqld.sock#' /etc/wallabag/parameters.template.yml && \
  sed -i '30,33d' entrypoint.sh

# Image cleanup
RUN \
  rm -rf /var/lib/apk/*

# Labels customization
LABEL wallabag_version ${WALLABAG_VERSION}
LABEL php_version ${PHP_VERSION}
LABEL maintainer "Joel LE CORRE <gitlab@sublimigeek.fr>"
